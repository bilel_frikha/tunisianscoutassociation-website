import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { from, Observable, observable } from "rxjs";
import { User } from "./user";
import { Join } from "./join";


@Injectable({
    providedIn: 'root'
})
export class userService{

    private apiServerUrl ='http://localhost:8080/contactusapi/v1/contactUs';

    constructor(private http: HttpClient) { }

    /** POST : add new feedback to the database */
    public addFeedback(user: User): Observable<User>{
        return this.http.post<User>(this.apiServerUrl, user);
    }


    private apiServerUrl1 ='http://localhost:8080/joinusapi/v1/joinUs';

    /** POST : add new feedback to the database */
    public addNewMember(join: Join): Observable<Join>{
        return this.http.post<Join>(this.apiServerUrl1, join);
    }
    
}