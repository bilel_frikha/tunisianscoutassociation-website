import { Component, OnInit } from '@angular/core';
import { userService } from 'app/user.service';
import * as Rellax from 'rellax';
import { User } from "app/user";
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';



@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  data : Date = new Date();
  focus;
  focus1;

  msgTrue = false;

  constructor(private userService: userService) { }

  ngOnInit() {
    var rellaxHeader = new Rellax('.rellax-header');

    var body = document.getElementsByTagName('body')[0];
    body.classList.add('landing-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
    
  }
  ngOnDestroy(){
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('landing-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }

  public users: User[];
  public user:User;


  public onAddFeedback(addForm: NgForm): void{
    document.getElementById('add-feedback-form').click();
    this.userService.addFeedback(addForm.value).subscribe(
      (response: User) =>{
        console.log(response);
      },
    (error: HttpErrorResponse) => {
      alert(error.message);
    }
    );
  }

  addNewFeedback(addForm){
    const newFormData = { name: addForm.value.name, email: addForm.value.email, message: addForm.value.message};

    this.userService.addFeedback(newFormData).subscribe(data => {
      console.log(data);
      this.msgTrue = true;
    });


  }






  public onOpenModal(user: User, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = "button";
    button.style.display = "none";
    button.setAttribute('data-toggle', 'modal');
    if (mode == 'add'){
      button.setAttribute('data-target', '#addFeedbackModal');
    }
    container.appendChild(button);
    button.click();
  }







}
