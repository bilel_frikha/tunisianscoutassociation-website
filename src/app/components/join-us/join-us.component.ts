import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { userService } from 'app/user.service';
import { NgForm } from '@angular/forms';
import { Join } from 'app/join';

@Component({
  selector: 'app-join-us',
  templateUrl: './join-us.component.html',
  styleUrls: ['./join-us.component.css']
})
export class JoinUsComponent implements OnInit {

  data : Date = new Date();
  focus;
  focus1;
  model: NgbDateStruct;
  public join:Join;
  msgTrue = false;

  constructor(private userService: userService) { }

  ngOnInit() {
      var body = document.getElementsByTagName('body')[0];
      body.classList.add('login-page');

      var navbar = document.getElementsByTagName('nav')[0];
      navbar.classList.add('navbar-transparent');
  }
  ngOnDestroy(){
      var body = document.getElementsByTagName('body')[0];
      body.classList.remove('login-page');

      var navbar = document.getElementsByTagName('nav')[0];
      navbar.classList.remove('navbar-transparent');
  }
  private dateToString = (date) => `${date.year}-${date.month}-${date.day}`; 

  addMember( joinForm){

    const newFormData1 = { 
      nom: joinForm.value.nom,
      prenom: joinForm.value.prenom,
      email: joinForm.value.email,
      dateDeNaissance: this.dateToString(joinForm.value.dateDeNaissance),
      delegation: joinForm.value.delegation,
      gouvernaurat: joinForm.value.gouvernaurat,
      address: joinForm.value.address,
      niveauEducatif: joinForm.value.niveauEducatif,
      vousEtes: joinForm.value.vousEtes,
      etablissement: joinForm.value.etablissement,
      specialite: joinForm.value.specialite,
      emploi: joinForm.value.emploi,
      numTelephone: joinForm.value.numTelephone,
      scout: joinForm.value.scout 
    };

    this.userService.addNewMember(newFormData1).subscribe(data => {
      console.log(data);
      this.msgTrue = true;
    });


  }




}
