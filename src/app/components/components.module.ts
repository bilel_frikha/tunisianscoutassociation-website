import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { RouterModule } from '@angular/router';

import { BasicelementsComponent } from './basicelements/basicelements.component';
import { NavigationComponent } from './navigation/navigation.component';
import { TypographyComponent } from './typography/typography.component';
import { NucleoiconsComponent } from './nucleoicons/nucleoicons.component';
import { ComponentsComponent } from './components.component';
import { NotificationComponent } from './notification/notification.component';
import { NgbdModalBasic } from './modal/modal.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { NewsComponent } from './news/news.component';
import { NewsDetailsComponent } from './news-details/news-details.component';
import { ProductsComponent } from './products/products.component';
import { JoinUsComponent } from './join-us/join-us.component';
import { SupportUsComponent } from './support-us/support-us.component';
import { OurGoalsComponent } from './our-goals/our-goals.component';
import { ParticularComponent } from './support-us/particular/particular.component';
import { EntrepriseComponent } from './support-us/entreprise/entreprise.component';
import { AssociationComponent } from './support-us/association/association.component';
import { WhoWeAreComponent } from './about-us/who-we-are/who-we-are.component';
import { LawComponent } from './about-us/law/law.component';
import { MissionComponent } from './about-us/mission/mission.component';
import { GovernanceComponent } from './about-us/governance/governance.component';
import { PoliticComponent } from './about-us/politic/politic.component';
import { HistoryComponent } from './about-us/history/history.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        NouisliderModule,
        RouterModule,
        JwBootstrapSwitchNg2Module
      ],
    declarations: [
        ComponentsComponent,
        BasicelementsComponent,
        NavigationComponent,
        TypographyComponent,
        NucleoiconsComponent,
        NotificationComponent,
        NgbdModalBasic,
        AboutUsComponent,
        ContactUsComponent,
        NewsComponent,
        NewsDetailsComponent,
        ProductsComponent,
        JoinUsComponent,
        SupportUsComponent,
        OurGoalsComponent,
        ParticularComponent,
        EntrepriseComponent,
        AssociationComponent,
        WhoWeAreComponent,
        LawComponent,
        MissionComponent,
        GovernanceComponent,
        PoliticComponent,
        HistoryComponent
    ],
    exports:[ ComponentsComponent ]
})
export class ComponentsModule { }
