import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsComponent } from './components/components.component';
import { LandingComponent } from './examples/landing/landing.component';
import { LoginComponent } from './examples/login/login.component';
import { ProfileComponent } from './examples/profile/profile.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import { AboutUsComponent} from './components/about-us/about-us.component';
import {ContactUsComponent} from './components/contact-us/contact-us.component';
import {NewsComponent} from './components/news/news.component';
import {NewsDetailsComponent} from './components/news-details/news-details.component';
import {ProductsComponent} from './components/products/products.component';
import {JoinUsComponent} from './components/join-us/join-us.component';
import {SupportUsComponent} from './components/support-us/support-us.component';
import {ParticularComponent} from './components/support-us/particular/particular.component';
import {OurGoalsComponent} from './components/our-goals/our-goals.component';
import {EntrepriseComponent} from './components/support-us/entreprise/entreprise.component';
import {AssociationComponent} from './components/support-us/association/association.component';
import { GovernanceComponent} from './components/about-us/governance/governance.component';
import { HistoryComponent} from './components/about-us/history/history.component';
import { LawComponent } from './components/about-us/law/law.component';
import { MissionComponent} from './components/about-us/mission/mission.component';
import { PoliticComponent} from './components/about-us/politic/politic.component';
import { WhoWeAreComponent} from './components/about-us/who-we-are/who-we-are.component';
const routes: Routes =[
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    { path: 'index',                component: ComponentsComponent },
    { path: 'nucleoicons',          component: NucleoiconsComponent },
    { path: 'examples/landing',     component: LandingComponent },
    { path: 'examples/login',       component: LoginComponent },
    { path: 'examples/profile',     component: ProfileComponent },
    { path: 'about-us',             component: AboutUsComponent},
    { path: 'governance',           component: GovernanceComponent},
    { path: 'history',              component: HistoryComponent},
    { path: 'law',                  component: LawComponent},
    { path: 'mission',              component: MissionComponent},
    { path: 'politic',              component: PoliticComponent},
    { path: 'who-we-are',           component: WhoWeAreComponent},
    { path: 'contact-us',           component: ContactUsComponent},
    { path: 'news',                 component: NewsComponent},
    { path: 'news-detail',          component: NewsDetailsComponent},
    { path: 'products',             component: ProductsComponent},
    { path: 'join-us',              component: JoinUsComponent},
    { path: 'support-us',           component: SupportUsComponent},
    { path: 'particular',           component: ParticularComponent},
    { path: 'entreprise',           component: EntrepriseComponent},
    { path: 'association',          component: AssociationComponent},
    { path: 'our-goals',            component: OurGoalsComponent}
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
