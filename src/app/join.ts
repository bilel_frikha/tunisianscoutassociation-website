export interface Join {
    nom: string;
    prenom: string;
    email: string;
    dateDeNaissance: Date;
    delegation: string;
    gouvernaurat: string;
    address: string;
    niveauEducatif: string;
    vousEtes: string;
    etablissement: string;
    specialite: string;
    emploi: string;
    numTelephone: string;
    scout: string;
    }